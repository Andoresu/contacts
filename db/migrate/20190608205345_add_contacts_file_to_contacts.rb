class AddContactsFileToContacts < ActiveRecord::Migration[5.2]
  def change
    add_reference :contacts, :contacts_file, foreign_key: true
  end
end
