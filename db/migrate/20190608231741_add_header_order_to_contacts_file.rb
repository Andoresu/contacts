class AddHeaderOrderToContactsFile < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts_files, :header_order, :string
  end
end
