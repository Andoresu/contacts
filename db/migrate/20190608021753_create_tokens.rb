class CreateTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :tokens do |t|
      t.string :token
      t.references :user, foreign_key: true
      t.datetime :expires_at

      t.timestamps
    end
    add_index :tokens, :token, unique: true
  end
end
