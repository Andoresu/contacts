class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :name
      t.date :birthday
      t.string :phone
      t.string :address
      t.string :credit_card_number
      t.string :credit_card_last_4_numbers
      t.string :credit_card_type
      t.string :email
      t.string :salt
      
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :contacts, [:user_id, :email], unique: true
  end
end
