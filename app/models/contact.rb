class Contact < ApplicationRecord
  
  belongs_to :user
  belongs_to :contacts_file
    
  validates :name, :birthday, :phone, :address, :credit_card_number, :email, presence: true
  validates :email, uniqueness: { scope: :user_id, case_sensitive: false }, format: { with: URI::MailTo::EMAIL_REGEXP } 
  validates :name, format: { with: /\A[a-zA-Z0-9 -]*\z/ }
  validates :phone, format: { with: /\A([(][+]\d{2}[)]( \d{3}){2}( \d{2}){2})|([(][+]\d{2}[)]( \d{3})(-\d{3})(-\d{2}){2})\z/ }
  
  validate :validate_birthday_format, :valid_credit_card?, on: :create
  
  def set_credit_card_last_4_numbers
    self.credit_card_last_4_numbers = self.credit_card_number[-4..-1]
  end
  
  def set_salt
     self.salt = BCrypt::Engine.generate_salt(1)
  end
  
  def encrypt_card
    self.credit_card_number = BCrypt::Engine.hash_secret(self.credit_card_number, self.salt)
  end
  
  def set_credit_card_type
    self.credit_card_type = CreditCardValidations::Detector.new(self.credit_card_number).brand_name
  end
  
  def valid_credit_card?
    if self.credit_card_number.present? && credit_card_valid?
      self.set_credit_card_last_4_numbers
      self.set_credit_card_type
      self.set_salt
      self.encrypt_card
    else
      errors.add(:credit_card_number, "invalid credit_card¿")
    end
  end
  
  private 
  
  def validate_birthday_format
    if self.birthday.present? && 
      (Date.strptime(self.birthday.to_s, '%Y%m%d') rescue nil).nil? &&
      (Date.strptime(self.birthday.to_s, '%F') rescue nil).nil?
      errors.add(:birthday, "invalid date format")
    end
  end
  
  def credit_card_valid? #Luhn algorithm
  
    digits = self.credit_card_number.chars.map(&:to_i)
    check = digits.pop
  
    sum = digits.reverse.each_slice(2).flat_map do |x, y|
      [(x * 2).divmod(10), y || 0]
    end.flatten.inject(:+)
  
    check.zero? ? sum % 10 == 0 : (10 - sum % 10) == check
  end
    
end
