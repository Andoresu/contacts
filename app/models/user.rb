class User < ApplicationRecord
  authenticates_with_sorcery!
  
  has_many :tokens, dependent: :destroy  
  has_many :contacts, dependent: :destroy  
  has_many :contacts_files, dependent: :destroy  
  
  
  attr_accessor :password, :password_confirmation, :token
  
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP } 
  validates :email, uniqueness: { case_sensitive: false }  
  validates :password, :password_confirmation, presence: true, on: :create
  validates :password, length: {minimum: 8}, confirmation: true, on: :create
  
  after_create :generate_token
  
  def generate_token
    token = Token.create!(user: self)
    self.token = token.token
  end
  
end
