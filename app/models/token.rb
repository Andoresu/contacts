class Token < ApplicationRecord

  has_secure_token :token
    
  belongs_to :user
  
  before_create :set_expires_at
  
  def is_valid?
    DateTime.now < self.expires_at
  end
  
  private
  
  def set_expires_at
    self.expires_at ||= 1.month.from_now
  end
  
end
