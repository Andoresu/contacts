class ContactsFile < ApplicationRecord
    
  include AASM
    
  belongs_to :user
  
  mount_uploader :csv_file, CsvUploader
  
  validates :csv_file, presence: true
  
  scope :processing,  -> (foo=true) { where(state: 'processing') }
  scope :failed,      -> (foo=true) { where(state: 'failed') }
  scope :waiting,     -> (foo=true) { where(state: 'waiting') }
  scope :finished,    -> (foo=true) { where(state: ['failed', 'done']) }
  
  def self.filters
    [:processing, :failed, :waiting, :finished]
  end
  
  
  aasm column: "state" do
    state :waiting, initial: true
    state :processing
    state :failed
    state :done

    event  :start do
      transitions  from: :waiting, to: :processing
    end

    event  :fail do
      transitions  from: :processing, to: :failed
    end
    
    event :success do
      transitions  from: :processing, to: :done
    end
  end
  
  def header_order_value=(value)
    if value.kind_of?(Array)
      self.header_order = value.join(',')
    else
      self.header_order = value
    end
  end
  
  def header_array
    self.header_order.split(',')
  end
  
end
