class NotificationMailer < ApplicationMailer
  def contacts_file_notification(contacts_file)
    @contacts_file = contacts_file
    mail to: contacts_file.user.email, subject: "Your file has already been processed"
  end
end
