class ApplicationMailer < ActionMailer::Base
  default from: 'no-replay@contactstest.com'
  layout 'mailer'
end
