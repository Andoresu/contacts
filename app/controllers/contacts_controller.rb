class ContactsController < ApplicationController
    
  before_action :verify_token, only: [:index]
  
  def index
    contacts = @user.contacts.super_filter(params)
    return render_collection("contacts", contacts, ContactSerializer)
  end
    
end
