class SessionsController < ApplicationController

  before_action :verify_token, only: [:logout, :check]

  def check
    render json: @user, status: :ok
  end

  def create

    email = session_params[:email]
    pass = session_params[:password]
    user = login(email, pass)
    
    if user
      user.generate_token
      render json: user, status: :created
    else
      render_json(:unauthorized, {error: 'Invalid credentials'})
    end

  end
  
  def logout
    @token.destroy
    render_json(:no_content)
  end
  
  private
    
  def session_params
    params.require(:user).permit(:email, :password)
  end

end
