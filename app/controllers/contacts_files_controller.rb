class ContactsFilesController < ApplicationController
    
  before_action :verify_token, only: [:index, :create]
  
  def index
    contacts_files = @user.contacts_files.super_filter(params)
    return render_collection("contacts_files", contacts_files, ContactsFileSerializer)
  end
  
  def create
    contacts_file = ContactsFile.new(contacts_file_params)
    contacts_file.user = @user
    if contacts_file.save
      ContactsWorker.perform_async(contacts_file.id)
      render json: contacts_file, status: :created
    else
      render_json(:unprocessable, {errors: contacts_file.errors})
    end
  end
  
  private
  
  def contacts_file_params
    params.require(:contacts_file).permit(:csv_file, :header_order_value => [])
  end
    
end
