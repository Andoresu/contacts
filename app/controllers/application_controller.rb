class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  
  def render_json(type, opts = {})
    case type
    when :created
      render json: opts, root: false, status: type
    when :unprocessable
      render json: opts, status: :unprocessable_entity
    when :no_content
      head type
    when :unauthorized
      if opts[:error].nil?
        opts[:error] = "Unauthorized"
      end
      render json: opts, status: type
    when :not_found      
      if opts[:error].nil?
        opts[:error] = "Not found"
      end
      render json: opts, status: type
    else
      render json: opts, status: type
    end
  end
  
  def render_collection(root, collection, serializer)
    json = {}
    json[root] = ActiveModelSerializers::SerializableResource.new(collection, each_serializer: serializer)
    json["total_count"] = collection.count
    render json: json, status: :ok
  end
  
  def set_user_by_token
    if request.headers.key?(:Authorization)
      @token = Token.find_by(token: request.headers['Authorization'])
      if @token && @token.is_valid?  
        @user = @token.user
        @user.token = @token.token
      end
    end
  end
  
  def verify_token
    set_user_by_token
    unless @user
      return render_json(:unauthorized)
    end
  end
  
  
  
end
