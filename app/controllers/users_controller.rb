class UsersController < ApplicationController
  
  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: :created
    else
      render_json(:unprocessable, {errors: user.errors})
    end
  end
    
  private
    
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
