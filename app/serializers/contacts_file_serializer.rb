class ContactsFileSerializer < ActiveModel::Serializer
  attributes :id, :csv_file, :user_id, :state, :err_message, :header_order
end
