class ContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :birthday, :phone, :address, :credit_card_last_4_numbers, 
            :credit_card_type, :email, :user_id, :created_at, :updated_at
  
  def birthday
    object.birthday.strftime("%Y %B %-d")
  end
  
end
