class ContactsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false
    
  def perform(contacts_file_id)
    
    contacts_file = ContactsFile.find_by(id: contacts_file_id)
    
    if contacts_file
      contacts_file.start!
      tmp_file = Tempfile.new([contacts_file_id.to_s, '.csv'])
      tmp_file.binmode
      
      open(contacts_file.csv_file.url) do |url_file|
        tmp_file.write(url_file.read)
      end
      
      tmp_file.rewind
  
      tmp_file.read 
      
      spreadsheet = Roo::CSV.new(tmp_file.path, csv_options: {col_sep: ";"})
      header = contacts_file.header_array
      attributes = header.slice(0,6)
      errors = []
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        contact = Contact.new
        contact.attributes = row.to_hash.slice(*attributes)
        contact.user_id = contacts_file.user_id
        contact.contacts_file_id = contacts_file_id
        unless contact.save
          errors << "Error in row #{i}: #{contact.errors.full_messages.join(', ')}"
        end
      end
      if errors.empty?
        contacts_file.success
      else
        contacts_file.err_message = errors.join("\n")
        contacts_file.fail
      end
      contacts_file.save
      NotificationMailer.contacts_file_notification(contacts_file).deliver_now
    end
    
    
    
  end
end