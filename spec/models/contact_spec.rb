require 'rails_helper'

RSpec.describe Contact, type: :model do
  context 'validation tests' do
      
    before(:all) do
      @user = create(:user)
      @contacts_file = create(:contacts_file, {user: @user})
      @contact = build(:contact, {contacts_file: @contacts_file, user: @user})
    end
    
    it "is valid with valid attributes" do
      expect(@contact).to be_valid
    end
    
    it 'is has a valid name' do
      @contact.name = "$asfasf"
      expect(@contact).to_not be_valid
    end
    
    it 'is has a valid birthday' do
      @contact.birthday = "01-01-2018"
      expect(@contact).to_not be_valid
    end
    
    it 'is has a valid phone' do
      @contact.phone = "(+57) 320-432 05-09"
      expect(@contact).to_not be_valid
    end
    
    it 'is has a valid address' do
      @contact.address = ""
      expect(@contact).to_not be_valid
    end
    
    it 'is has a valid credit card number' do
      @contact.credit_card_number = "37144963539843"
      expect(@contact).to_not be_valid
    end
    
    it 'is has a encrypted credit card number' do
      @user = create(:user)
      @contact = build(:contact, {contacts_file: @contacts_file, user: @user})
      credit_card_number = @contact.credit_card_number
      expect(@contact).to be_valid
      expect(@contact.credit_card_number).to_not eq(credit_card_number)
    end
    
    it 'is has last 4 card numbers' do
      @user = create(:user)
      @contact = build(:contact, {credit_card_number: "371449635398431", contacts_file: @contacts_file})
      @contact.user = @user
      expect(@contact).to be_valid
      expect(@contact.credit_card_last_4_numbers).to eq("8431")
    end
    
    it 'ensures email presence' do
      @contact.email = nil
      expect(@contact).to_not be_valid
    end
    
    it 'ensures email unique' do
      @user = create(:user)
      @contact = build(:contact, {email: "foo@foo.com", contacts_file: @contacts_file})
      @contact.user = @user
      expect(@contact.save).to eq(true)
      @contact = build(:contact, {email: "foo@foo.com", contacts_file: @contacts_file})
      @contact.user = @user
      expect(@contact).to_not be_valid
    end
    
    
      
  end
end
