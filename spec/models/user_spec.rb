require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validation tests' do
    before(:all) do
      @user = build(:user)
    end
    
    it "is valid with valid attributes" do
      expect(@user).to be_valid
    end
    
    it 'ensures email presence' do
      @user.email = nil
      expect(@user).to_not be_valid
    end
    
    it 'ensures has a email unique' do
      @user = build(:user)
      @user.email.upcase!
      expect(@user.save).to eq(true)
      @user = build(:user, email: @user.email.downcase)
      expect(@user).to_not be_valid
    end
    
    it 'ensure password presence' do
      @user.password = nil
      expect(@user).to_not be_valid
    end
    
    it 'ensure password_confirmation presence' do
      @user.password_confirmation = nil
      expect(@user).to_not be_valid
    end
    
    it 'ensure password_confirmation validation' do
      @user.password = '12345678'
      @user.password_confirmation = '1234567'
      expect(@user).to_not be_valid
    end
    
    it 'ensure can be saved' do
      @user = build(:user)
      expect(@user.save).to eq(true)
    end
    
    it 'validates has token' do
      @user = create(:user)
      @token = @user.tokens.last
      expect(@token).to_not eq(nil)
    end
    
    it 'validates has a valid token' do
      @user = create(:user)
      @token = @user.tokens.last
      expect(@token.is_valid?).to eq(true)
    end    
  end
end
