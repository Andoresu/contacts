FactoryBot.define do
  factory :contacts_file do
    csv_file { File.open("#{Rails.root}/public/contactos.csv") }
    header_order { "name,birthday,phone,address,credit_card_number,email" }
  end
end
