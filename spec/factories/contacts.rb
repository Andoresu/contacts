FactoryBot.define do
  factory :contact do
    name { Faker::Name.name.gsub!(/[^0-9A-Za-z]/, '') }
    birthday { "2019/06/08" }
    phone { "(+57) 320-432-05-09" }
    address { "asfsf" }
    credit_card_number { "371449635398431" }
    email { Faker::Internet.email }
  end
end
