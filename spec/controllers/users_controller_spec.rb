require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  context 'POST #create' do
    let(:user_params) do 
      {
        email: Faker::Internet.email,
        password: "12345678",
        password_confirmation: "12345678"
      }
    end
    it 'return a success response' do
      expect { post :create, params: {user: user_params} }.to change(User, :count).by(+1)
      expect(response).to have_http_status :created
    end
    it 'return a error response' do
      post :create, params: {user: {email: 'asf'}}
      expect(response).to have_http_status :unprocessable_entity
    end
  end
end
