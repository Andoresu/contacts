require 'rails_helper'

RSpec.describe ContactsController, type: :controller do
    
  before(:all) do
    @user = create(:user)
    @contacts_file = create(:contacts_file, {user: @user})
  end
    
  context 'GET #index' do
    it 'render index' do
      contact = create(:contact, {user: @user, contacts_file: @contacts_file})
      request.headers.merge!({ 'Authorization': @user.token })
      get :index
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body)
      expect(body['contacts'].length).to eq(1)
      expect(body['total_count']).to eq(1)
    end
  end

end
