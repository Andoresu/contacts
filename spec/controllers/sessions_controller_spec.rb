require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  before(:all) do
    @user = create(:user)
  end
  context 'POST #create' do
    it 'return a success response' do
        expect { post :create, params: {user: {email: @user.email, password: "12345678"}} }.to change(Token, :count).by(+1)
        expect(response).to have_http_status :created
    end
    it 'return a error response' do
        post :create, params: {user: {email: 'asf'}}
        expect(response).to have_http_status :unauthorized
    end
  end
  
  context 'GET #chek' do
    it 'return a success response' do
        request.headers.merge!({ 'Authorization': @user.token })
        get :check
        expect(response).to have_http_status :ok
    end
    it 'return a error response' do
        get :check
        expect(response).to have_http_status :unauthorized
    end
  end
  
   context 'DESTROY #logout' do
    it 'return a success response' do
        request.headers.merge!({ 'Authorization': @user.token })
        delete :logout
        expect(response).to have_http_status :no_content
    end
    it 'return a error response' do
        delete :logout
        expect(response).to have_http_status :unauthorized
    end
  end
end
