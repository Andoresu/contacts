Rails.application.routes.draw do
  scope defaults: {format: :json} do
    resources :users, only: [:create]
    resources :sessions, only: [:create] do
      collection do
        get 'check'
        delete 'logout'
      end
    end
    resources :contacts, only: [:index]
    resources :contacts_files, only: [:index, :create]
  end
end
