Sidekiq::Extensions.enable_delay!
Sidekiq.configure_server do |config|
  config.redis = { :url => ENV['REDIS_PROVIDER']}
  config.redis = { :size => 20 }
end

Sidekiq.configure_client do |config|
  config.redis = { :url => ENV['REDIS_PROVIDER']}
  config.redis = { :size => 1 }
end