### Features

- creacion de usuarios (registro)
- inicio de sesion
- crear contactos a partir de un archivo csv seperado por ; de 6 columnas
- creacion de contactos en background con sidekiq
- notificacion de por email cuando el archivo sea procesado
- almacenamientos de arhivos en s3
- testing para modelos y controladores (exepto contacts_file model y controller)

# Contactos

**Documentacion en postman**

todos los endpoints y variables usadas estan registradas en la documentacion de postman.

[Link](https://documenter.getpostman.com/view/137564/S1TZxvZi?version=latest#1cbd7dc3-caa0-49e7-af92-cfd4e7a00394/ "link title")

**Requerimientos para correr localmente el proyecto**

- Rails 5.2.3
- Ruby 2.6.3
- postgresql
- redis-server (runing for sidekiq)
- definir las siguientes variables de entorno: AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY, AWS_BUCKET, AWS_BUCKET, AWS_REGION, SENGRID_USERNAME, SENGRID_PASSWORD
- (optional) para que el mailer funcione las credenciales de una cuenta de sendfrid como variables de entorno: SENGRID_USERNAME, SENGRID_PASSWORD

####ejecutar los siguientes comandos

`$ redis-server`

`$ bundle exec sidekiq -e ${RACK_ENV:-development} -c 5`

`$ bundle exec puma -v`

####ejecutar todos los test
`$ rspec`

####Notas
- la autotenticacion utilizada fue por token las todos los request deben llevar un header `Authorization` con el token (exeptuando el metodo del login y registro)

- [Link](https://dashboard.heroku.com/apps/contacts-koombea-test/resources/) de heroku

- [Archivo de prueba](https://bitbucket.org/Andoresu/contacts/src/master/public/contactos.csv/) del repositorio